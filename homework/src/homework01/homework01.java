package homework01;

import org.junit.Test;

import java.util.Arrays;

/*
    Name -
    Class -
    Date -

    Description -
 */
public class homework01 {

    public int sum(int... nums){
        return Arrays.stream(nums).sum();
    }

    public int sumAgain(int... nums){
        int total = 0;

        for(int j : nums){
            total += j;
        }
        return total;
    }
}
