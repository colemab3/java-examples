package module01;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void sayThing() {
        assertEquals(" 0. Test\n", module01.Main.sayThing("Test",0));
    }
}